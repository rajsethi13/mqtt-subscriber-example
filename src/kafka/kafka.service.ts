import { Inject, Injectable } from '@nestjs/common';
import { KAFKA_CONNECTION } from './kafka.provider';
import { Producer } from 'kafkajs';

@Injectable()
export class KafkaService {
  constructor(@Inject(KAFKA_CONNECTION) private readonly producer: Producer) {}

  async publish(event) {
    await this.producer.connect();
    await this.producer.send({
      topic: 'rauth-stream',
      messages: [{ value: JSON.stringify(event) }],
    });
    await this.producer.disconnect();
  }
}
