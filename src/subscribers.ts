import { Transport } from '@nestjs/microservices';
import { INestApplication } from '@nestjs/common';
import { TCPCustomTransport } from './server-tcp';

export const EVENTS_PORT = '1883';
export const EVENTS_HOST = 'event-bus';
export const MQTT_PROTO = 'mqtt';
export const TCP_PROTO = 'tcp';
export const EVENTS_USER = 'events_user';
export const EVENTS_PASSWORD = 'events_password';
export const TCP_EVENTS_PORT = '9000';
export const TCP_EVENTS_HOST = '0.0.0.0';

export async function listenToMQTT(app: INestApplication) {
  const protocol = process.env.EVENTS_PROTO || MQTT_PROTO;
  const user = process.env.EVENTS_USER || EVENTS_USER;
  const password = process.env.EVENTS_PASSWORD || EVENTS_PASSWORD;
  const host = process.env.EVENTS_HOST || EVENTS_HOST;
  const port = process.env.EVENTS_PORT || EVENTS_PORT;
  const mqttUrl = `${protocol}://${user}:${password}@${host}:${port}`;
  const microservice = app.connectMicroservice({
    transport: Transport.MQTT,
    options: {
      url: mqttUrl,
      clientId: process.env.EVENTS_CLIENT_ID,
      protocolVersion: 5,
    },
  });
  await microservice.listen();
}

export async function listenToTCP(app: INestApplication) {
  const microservice = app.connectMicroservice({
    strategy: new TCPCustomTransport({
      host: process.env.EVENTS_HOST || TCP_EVENTS_HOST,
      port: Number(process.env.EVENTS_PORT || TCP_EVENTS_PORT),
    }),
  });
  await microservice.listen();
}

export async function listenToEvents(app: INestApplication) {
  if (process.env.EVENTS_PROTO === MQTT_PROTO) {
    await listenToMQTT(app);
  }
  await listenToTCP(app);
}
