import { Document } from 'mongoose';

export interface Event extends Document {
  eventId: string;
  eventName: string;
  eventFromService: string;
  eventDateTime: Date;
  eventData: unknown;
}
