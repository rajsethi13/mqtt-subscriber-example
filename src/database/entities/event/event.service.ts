import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { Event } from './event.interface';
import { EVENT } from './event.schema';

@Injectable()
export class EventService {
  constructor(
    @Inject(EVENT)
    private model: Model<Event>,
  ) {}

  async create(payload: Event) {
    return await this.model.findOneAndUpdate(
      { eventId: payload.eventId },
      payload,
      { upsert: true },
    );
  }
}
