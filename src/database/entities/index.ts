import { Connection } from 'mongoose';
import { EVENT, EventSchema } from './event/event.schema';
import { MONGOOSE_CONNECTION } from '../database.provider';
import { EventService } from './event/event.service';

export const EventsEntities = [
  {
    provide: EVENT,
    useFactory: (connection: Connection) =>
      connection.model(EVENT, EventSchema),
    inject: [MONGOOSE_CONNECTION],
  },
  EventService,
];
