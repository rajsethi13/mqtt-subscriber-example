import { Logger } from '@nestjs/common';
import * as mongoose from 'mongoose';
import {
  Observable,
  catchError,
  defer,
  lastValueFrom,
  retry,
  throwError,
} from 'rxjs';

export const MONGOOSE_CONNECTION = 'MONGOOSE_CONNECTION';
export const DB_PROTOCOL = 'mongodb';

export const MongooseProvider = {
  provide: MONGOOSE_CONNECTION,
  useFactory: async (): Promise<typeof mongoose> => {
    const protocol = process.env.DB_PROTOCOL || DB_PROTOCOL;
    const user = process.env.DB_USER || 'smsuser';
    const password = process.env.DB_PASSWORD || 'smspassword';
    const dbHost = process.env.DB_HOST || 'mongo';
    const dbName = process.env.DB_NAME || 'smsdb';

    const mongoOptions = 'retryWrites=true';
    mongoose.set('strictQuery', false);
    return await lastValueFrom(
      defer(() =>
        mongoose.connect(
          `${protocol}://${user}:${password}@${dbHost.replace(/,\s*$/, '')}/${dbName}?${mongoOptions}`,
        ),
      ).pipe(handleRetry()),
    );
  },
};

export function handleRetry(
  count = 9,
  delay = 3000,
): <T>(source: Observable<T>) => Observable<T> {
  return <T>(source: Observable<T>) =>
    source.pipe(
      retry({ count, delay }),
      catchError((e) => {
        Logger.error(
          `Unable to connect to the database. Retrying...`,
          '',
          MONGOOSE_CONNECTION,
        );
        return throwError(() => new Error(e));
      }),
    );
}
