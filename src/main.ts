import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { listenToEvents } from './subscribers';

export const PORT = process.env.PORT || '8000';
export const API_PREFIX = 'api';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.setGlobalPrefix(API_PREFIX);
  await listenToEvents(app);
  await app.listen(PORT);
}
bootstrap();
