import { Controller, Logger } from '@nestjs/common';
import { EventPattern } from '@nestjs/microservices';
import { EventService } from '../database/entities/event/event.service';
import { KafkaService } from '../kafka/kafka.service';

export const MQTT_WILDCARD = '+';

@Controller('event')
export class EventController {
  private logger = new Logger();

  constructor(
    private readonly event: EventService,
    private readonly kafka: KafkaService,
  ) {}

  @EventPattern(MQTT_WILDCARD)
  catchAll(payload) {
    this.logger.log(payload, this.constructor.name);

    this.kafka
      .publish(payload)
      .catch((error) => this.logger.error(error, error, this.constructor.name))
      .then(() =>
        this.logger.log(
          `${payload.eventName} produced`,
          this.kafka.constructor.name,
        ),
      );

    this.event
      .create(payload)
      .then(() =>
        this.logger.log(
          `${payload.eventName} stored`,
          this.event.constructor.name,
        ),
      )
      .catch((error) => this.logger.error(error, error, this.constructor.name));
  }
}
